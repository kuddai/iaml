=== Run information ===

Scheme:       weka.classifiers.functions.LinearRegression -S 1 -C -R 1.0E-8
Relation:     train_auto_PartB_numeric-weka.filters.unsupervised.attribute.AddExpression-Ea6*a9-Nexpression-weka.filters.unsupervised.attribute.Remove-R17-weka.filters.unsupervised.attribute.AddExpression-Ea6*a12-Nexpression-weka.filters.unsupervised.attribute.Remove-R17-weka.filters.unsupervised.attribute.AddExpression-Ea6*a13-Nexpression
Instances:    159
Attributes:   17
              normalized-losses
              wheel-base
              length
              width
              height
              engine-size
              bore
              stroke
              compression-ratio
              engine-power
              peak-rpm
              city-mpg
              highway-mpg
              mean-effective-pressure
              torque
              price
              a6*a13
Test mode:    5-fold cross-validation

=== Classifier model (full training set) ===


Linear Regression Model

price =

     -3.8811 * normalized-losses +
     20.0643 * wheel-base +
     32.0616 * length +
    703.413  * width +
    207.7602 * height +
    -94.5511 * engine-size +
   -579.4064 * bore +
  -1616.4302 * stroke +
    190.8752 * compression-ratio +
     -0.0123 * engine-power +
      1.2992 * peak-rpm +
     43.8693 * city-mpg +
   -408.9775 * highway-mpg +
    -18.6577 * mean-effective-pressure +
      0.0917 * torque +
     15.3127 * a6*a13 +
 -45043.3585

Time taken to build model: 0 seconds

=== Cross-validation ===
=== Summary ===

Correlation coefficient                  0.7581
Mean absolute error                   3001.9091
Root mean squared error               4592.4629
Relative absolute error                 60.8379 %
Root relative squared error             67.9072 %
Total Number of Instances              159     

